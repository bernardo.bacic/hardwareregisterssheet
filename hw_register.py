#!/usr/bin/env python3

#
# Hardware register spreadsheet:
#   https://docs.google.com/spreadsheets/d/1ZyNmHjPn7D72vfRK7V-OW74nCNmp5qUENK4CHXJLGjc/
#
import os.path

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

# If modifying these scopes, delete the file token.json.
SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
HWREG_SPREADSHEET_ID = '1ZyNmHjPn7D72vfRK7V-OW74nCNmp5qUENK4CHXJLGjc'
SHEET_NAME = 'Perentie01 - Card mappings'
PERENTIE1_CELL_RANGE = f'{SHEET_NAME}!B4:K11'
PERENTIE2_CELL_RANGE = f'{SHEET_NAME}!B27:K34'

OAUTH_FILE = 'credentials.json' # FIXME how to get it

class HardwareRegister():
    TOKEN_FILE = 'token.json'

    def __init__( self ):
        self.creds = None
        # The file token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists( self.TOKEN_FILE ):
            self.creds = Credentials.from_authorized_user_file( self.TOKEN_FILE, SCOPES)
            # If there are no (valid) credentials available, let the user log in.
        if not self.creds or not hasattr( self.creds, 'valid' ) or not self.creds.valid:
            #print( f'credentials valid: {self.creds.valid}' )
            if self.creds and self.creds.expired and self.creds.refresh_token:
                print( 'running self.creds.refresh(Request())' )
                self.creds.refresh(Request())
            else:
                print( 'running InstalledAppFlow.from_client_secrets_file( OAUTH_FILE, SCOPES )')
                flow = InstalledAppFlow.from_client_secrets_file( OAUTH_FILE, SCOPES )
                self.creds = flow.run_local_server(port=0)
                # Save the credentials for the next run
            with open( self.TOKEN_FILE, 'w') as token:
                print( f'writing to  {self.TOKEN_FILE}' )
                token.write(self.creds.to_json())

    def get_alveo_info(self) -> list:
        'Retrieve Alveo details from the spreadsheet.'
        try:
            service = build('sheets', 'v4', credentials=self.creds)
            # Call the Sheets API
            sheet = service.spreadsheets()
            result = sheet.values().get( spreadsheetId=HWREG_SPREADSHEET_ID,
                                         range=PERENTIE1_CELL_RANGE ).execute()
            #print( 'P1:', result.get('values', []) )
            values = result.get('values', [])
            # fetch PERNETIE2 Alveos as well
            result = sheet.values().get( spreadsheetId=HWREG_SPREADSHEET_ID,
                                         range=PERENTIE2_CELL_RANGE ).execute()
            p2_values = result.get('values', [])
            if len( values ) >= len( p2_values ):
                for i,row in enumerate( p2_values ):
                    values[i].extend( p2_values[i] )

            if not values:
                print('No data found.')
                return
            return self._as_dict( values )
        except HttpError as err:
            print(err)
            return []

    def _as_dict( self, rows ):
        '''Build a list of dictionaries {serial_nr:.., bdf:.., port:..}
        out of selected spreadsheet rows.
        '''
        lst = list( zip( rows[0], rows[2], rows[7] ) )
        # FILTER out empty entries (e.g. port empty when not connected to the switch)
        lst2 = [ i for i in lst if ' ' not in (i[0], i[2]) ]
        return [ {'serial_nr': sn, 'bdf': bdf, 'port':int(port)} for sn, bdf, port in lst2 ]
