FROM python:3.10


RUN pip3 install               \
	  google-api-python-client \
	  google-auth-httplib2     \
      google-auth-oauthlib
COPY *.py *.json /

CMD [ "python3","/web_server.py" ]
#
# build:
#	podman build --tag <version> .
#   `-> docker does not work anymore 🤷
#
# run:
#  podman run -it --publish "8080:8080" localhost/1
#
# stop
#  podman container rm <id>

# https://docs.docker.com/language/python/build-images/
# https://yvescallaert.medium.com/docker-intro-building-a-python-3-image-62031d0b7e39
