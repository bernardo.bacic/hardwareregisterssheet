#!/usr/bin/env python3

from http.server import BaseHTTPRequestHandler, HTTPServer
from hw_register import HardwareRegister
import json
import re
import time

# email.message.Message
#https://www.rose-hulman.edu/class/cs/archive/csse120-old/csse120-old-terms/201110/Resources/python-3.1.2-docs-html/library/email.message.html#email.message.Message

hostName = "0.0.0.0"
serverPort = 8080

class SimpleWebServer( BaseHTTPRequestHandler ):
    STALE_TRESHOLD_SEC = 60 # fetch info from spreadsheet if info oder than this many seconds

    def __init__( self, request, client_address, server ):
        'constructor'
        self.hw_reg = HardwareRegister()
        self.alveo_info = None
        self.last_update = 0
        BaseHTTPRequestHandler.__init__(self, request, client_address, server)

    def do_GET(self):
        self._set_headers()
        #self.send_response(200)
        #self.send_header("Content-type", "text/plain")
        #self.end_headers()
        #self.wfile.write( bytes( '{ "serial_nr": "XFL1RCFEG244", "port": 60, "BDF": "0000:b2:00.1" }',
                                 #"utf-8") )
        self.wfile.write( bytes( json.dumps( self._get_alveo_info()), "utf-8") )

        # FIXME handle /switch_ip_address
    def do_POST(self):
        self._set_headers()
        #print( f'\npath {self.path}' )
        if len( self.path ) < 2:
            self.do_GET()
            return
        alveo = json.dumps( self._find_alveo( self.path[1:] ) )
        print( f'alveo {alveo}' )
        self.wfile.write( bytes( alveo, 'utf-8'))
        #$ curl -X POST 202.9.15.136:8080/key=0000:b2:00.1
        #received post request: key=0000:b2:00.1


    def _find_alveo( self, key ):
        'key can be a serial#, BDF or P4 port'
        m = re.match( r'^[\d]+$', key )  # consists of digits (port #)
        port = int( key ) if m else 0
        for alveo in self._get_alveo_info():
            if key in (alveo[ 'serial_nr' ], alveo[ 'bdf' ]) or \
               port > 0 and port == alveo['port']:
                return alveo
        return { 'serial_nr': 'na', 'bdf': 'na', 'port': 0 }

    def _get_alveo_info(self):
        now = time.time()
        if self.alveo_info is None or \
           now > self.last_update + self.STALE_TRESHOLD_SEC:
            self.alveo_info = self.hw_reg.get_alveo_info()
        return self.alveo_info

    def _set_headers(self):
        # FIXME needed?
        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()


if __name__ == "__main__":
    webServer = HTTPServer((hostName, serverPort), SimpleWebServer)
    print("Server started http://%s:%s" % (hostName, serverPort))

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
